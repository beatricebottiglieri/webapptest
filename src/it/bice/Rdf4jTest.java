package it.bice;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

import java.util.List;

public class Rdf4jTest {

    public String generateTriple(String s) {
        // Abbiamo bisogno di una ValueFactory per istanziare oggetti di tipo Value (o sue sottoinferfacce)
        ValueFactory vf = SimpleValueFactory.getInstance();

        // Usiamo la value factory per costruire un IRI. � possibile costruire anche literal, bnode e
        // statement (come dimostrato pi� sotto)
        IRI test = vf.createIRI("http://example.org/"+s);

        // Costruisco uno statement che afferma: "socrate � una persona"
        Statement st = vf.createStatement(test, RDF.TYPE, FOAF.PERSON);

        return st.toString();
    }
}
